﻿import json
import openpyxl as xl
import os.path
class Record:
    def to_JSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, ensure_ascii=False)

def row2json(row):
    rec = Record()
    rec.number = str(row[0].value)
    rec.status = str(row[1].value)
    rec.update_date = str(row[2].value)
    rec.organization_kind = str(row[3].value)
    rec.organization_form = str(row[4].value)
    rec.full_name = str(row[5].value)
    rec.short_name = str(row[6].value)
    rec.social = str(row[7].value)
    rec.index = str(row[8].value)
    rec.region = str(row[9].value)
    return rec
s = input('Input the path to the file: ')
if s=='' and os.path.isfile(r"C:\Users\Саша\Desktop\Книга1.xlsx"):
    file = r"C:\Users\Саша\Desktop\Книга1.xlsx"
elif os.path.isfile(s):
    file = s
else:
    print('The file name is wrong.')
    input()
    exit()
print("The file is loading...")
wb = xl.load_workbook(filename=file)
ws = wb.active
json_records = []

for row in range(4,14):
    json_records.append(row2json(ws.rows[row]))
with open(r'C:\data.txt', 'w') as json_file:
    data = json.dumps(json_records, ensure_ascii=False,default=lambda o: o.__dict__)
    json_file.write(data)
print('The file is converted into JSON. The path to the JSON file is C:\data.txt')